#pragma once

template <typename _T>
class Heap
{
#define MIN(X,Y,Z)	(((X)<(Y))?(((X)<(Z))?(X):(Z)):(((Y)<(Z))?(Y):(Z)))
#define MAX(X,Y,Z)	(((X)>(Y))?(((X)>(Z))?(X):(Z)):(((Y)>(Z))?(Y):(Z)))
public:
	Heap(bool p_smallroot, int p_size=10):
	  m_smallroot(p_smallroot),m_length(p_size),m_current(0) {
		  m_data = new _T[m_length];
	  }
	  ~Heap() {
		  delete [] m_data;
	  }

protected:
	bool reallocate(const int length) {
		_T *tmp = m_data;
		m_data = new _T[length];
		if (!m_data) {
			m_data = tmp;
			return false;
		}
		memcpy(m_data, tmp, sizeof(_T)*m_length);
		delete [] tmp;
		m_length = length;
		return true;
	}
	bool isok(const int up, const int down) {
		if (up<0 || down>=m_current) return true;
		if (m_smallroot && m_data[up]<m_data[down]) return true;
		if (!m_smallroot && m_data[up]>m_data[down]) return true;
		return false;
	}
	void swap(_T &x, _T &y) {
		_T m=x; x=y; y=m;
	}
	_T& fit(_T &x, _T &y, _T &z) {
		if (m_smallroot) return MIN(x,y,z);
		else return MAX(x,y,z);
	}
public:
	bool push(_T x) {
		if (m_current==m_length) 
			if (!reallocate(m_length*2)) return false;
		m_data[m_current++] = x;
		int i = m_current-1;
		while (i>0) {
			if (!isok((i-1)/2,i)) swap(m_data[(i-1)/2], m_data[i]);
			i = (i-1)/2;
		}
		return true;
	}
	_T pop() {
		if (m_current==0) return 0;
		_T ret = m_data[0];
		int i=0;
		m_data[0] = m_data[--m_current];
		while (i*2+1<m_current) {
			_T &tmp = (i*2+2<m_current)?
				(fit(m_data[i], m_data[i*2+1], m_data[i*2+2])):
			(fit(m_data[i], m_data[i*2+1], m_data[i]));
			if (m_data[i]!=tmp) swap(m_data[i], tmp);
			if (m_data[i*2+1]==m_data[m_current]) i=i*2+1;
			else i=i*2+2;
		}
		return ret;
	}
	int size() {
		return m_current;
	}

private:
	bool m_smallroot;
	int m_length, m_current;
	_T *m_data;
};