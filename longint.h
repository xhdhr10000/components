#include <memory.h>
#include <cmath>

#define DIGIT		500
#define MAX(X,Y)	(((X)>(Y))?(X):(Y))
#define MIN(X,Y)	(((X)<(Y))?(X):(Y))

void minus(const int *x, const int *y, int *z);

bool great_or_equal(const int *x, const int *y, const int length)
{
	int i;
	i=length;
	while (x[i]==y[i] && i>0) i--;
	if (i==0 || x[i]>y[i]) return true;
	return false;
}

void add(const int *x, const int *y, int *z)
{
	memset(z, 0, sizeof(int)*DIGIT);
	if (x[0]*y[0]<0) {
		int big[DIGIT], small[DIGIT], negetive=1;
		if (great_or_equal(x, y, MAX(abs(x[0]),abs(y[0])))) {
			memcpy(big, x, sizeof(big));
			memcpy(small, y, sizeof(small));
		} else {
			memcpy(big, y, sizeof(big));
			memcpy(small, x, sizeof(small));
		}
		if (big[0]<0) {
			big[0] = -big[0];
			negetive = -1;
		} else small[0] = -small[0];
		minus(big, small, z);
		z[0] *= negetive;
	} else {
		z[0] = MAX(abs(x[0]),abs(y[0]));
		for (int i=1; i<=z[0]; i++) {
			z[i+1] += (x[i]+y[i]+z[i])/10;
			z[i] = (x[i]+y[i]+z[i])%10;
			if (i==z[0] && z[i+1]>0) z[0]++;
		}
		if (x[0]<0) z[0] = -z[0];
	}
}

void minus(const int *x, const int *y, int *z)
{
	memset(z, 0, sizeof(int)*DIGIT);
	if (x[0]*y[0]<0) {
		int t[DIGIT];
		if (x[0]<0) {
			memcpy(t, x, sizeof(t));
			t[0] = -t[0];
			add(t, y, z);
			z[0] = -z[0];
		} else {
			memcpy(t, y, sizeof(t));
			t[0] = -t[0];
			add(x, t, z);
		}
	} else {
		z[0] = MAX(abs(x[0]),abs(y[0]));
		if (!great_or_equal(x, y, z[0])) {
			minus(y, x, z);
			z[0] = -z[0];
		} else {
			int i=1;
			memcpy(z, x, sizeof(int)*DIGIT);
			while (i<=abs(y[0])) {
				while (z[i]<y[i]) {
					z[i+1]--;
					z[i]+=10;
				}
				z[i]-=y[i];
				i++;
			}
			while (z[i]<0) {
				z[i+1]--;
				z[i]+=10;
			}
			while (z[0]>1 && z[abs(z[0])]==0) z[0]--;
		}
	}
}

void multiply(const int *x, const int *y, int *z)
{
	int i, j;
	memset(z, 0, sizeof(int)*DIGIT);
	for (i=1; i<=abs(x[0]); i++) {
		for (j=1; j<=abs(y[0]); j++) z[i+j-1] += x[i]*y[j];
	}
	i=1;
	while (i<=abs(x[0])+abs(y[0])-1 || z[i]>0) {
		if (z[i]>=10) {
			z[i+1] += z[i]/10;
			z[i] %= 10;
		}
		i++;
	}
	z[0] = i-1;
	while (z[0]>1 && z[z[0]]==0) z[0]--;
	if (x[0]*y[0]<0) z[0] = -z[0];
}

void divide(const int *x, const int *y, int *z)
{
	int i, ii, j, k;
	int t[DIGIT];
	memset(z, 0, sizeof(int)*DIGIT);
	if (great_or_equal(&x[abs(x[0])-abs(y[0])], y, abs(y[0]))) {
		z[0] = abs(x[0])-abs(y[0])+1;
		i=abs(x[0]);
	} else {
		z[0] = abs(x[0])-abs(y[0]);
		i=abs(x[0])-1;
	}

	if (z[0]<=0) {
		z[0]=1;
		z[1]=0;
		return;
	}
	memcpy(t, x, sizeof(t));
	j=abs(y[0]); k=z[0];
	while (i>0 && i>=j) {
		while (j>0) {
			if (t[i]<y[j]) {
				t[i+1]--;
				ii=i+1;
				while (t[ii]<0) {
					t[ii+1]--;
					t[ii]+=10;
					ii++;
				}
				t[i]+=10;
			}
			t[i]-=y[j];
			i--; j--;
		}
		z[k]++;
		while (i>=0 && !great_or_equal(&t[i], y, DIGIT-i-1)) { i--; k--; }
		j=abs(y[0]); i+=abs(y[0]);
	}
	if (x[0]*y[0]<0) z[0] = -z[0];
}