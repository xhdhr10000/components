#include <iostream>
#include <ctime>
#include <memory.h>

using namespace std;

template <typename _T>
class Node
{
public:
	Node():left(-1),right(-1) {}
	Node(_T pkey):key(pkey) { Node(); }
	_T key;
	int left, right, weight;
};

template <typename _T>
class Treap
{
protected:
	void rotate_l(int &pos);
	void rotate_r(int &pos);
	bool enlarge();
public:
	Treap();
	~Treap();
	bool insert(int &pos, _T key);
	bool remove(int &pos, _T key);
	void print(int &pos);
	Node<_T> *node;
	int root, size, maxsize;
};