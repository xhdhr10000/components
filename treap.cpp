#include "treap.h"

template <typename _T>
Treap<_T>::Treap()
{
	srand(unsigned int(time(0)));
	root=-1;
	size=-1;
	maxsize=10;
	node = new Node<_T>[10];
}

template <typename _T>
Treap<_T>::~Treap()
{
	delete [] node;
}

template <typename _T>
void Treap<_T>::rotate_l(int &pos)
{
	int t = node[pos].right;
	node[pos].right = node[t].left;
	node[t].left = pos;
	pos = t;
}

template <typename _T>
void Treap<_T>::rotate_r(int &pos)
{
	int t = node[pos].left;
	node[pos].left = node[t].right;
	node[t].right = pos;
	pos = t;
}

template <typename _T>
bool Treap<_T>::enlarge()
{
	Node<_T> *tmp = node;
	node = new Node<_T>[maxsize*2];
	if (!node) {
		node = tmp;
		return false;
	}
	memcpy(node, tmp, sizeof(Node<_T>)*maxsize);
	maxsize *= 2;
	return true;
}

template <typename _T>
bool Treap<_T>::insert(int &pos, _T key)
{
	if (pos==-1) {
		if (size+1>=maxsize && !enlarge()) return false;
		size++;
		node[size].key = key;
		node[size].left = node[size].right = -1;
		node[size].weight = rand();
		pos = size;
	} else if (key <= node[pos].key) {
		if (!insert(node[pos].left, key)) return false;
		if (node[node[pos].left].weight>node[pos].weight) rotate_r(pos);
	} else {
		if (!insert(node[pos].right, key)) return false;
		if (node[node[pos].right].weight>node[pos].weight) rotate_l(pos);
	}
	return true;
}

template <typename _T>
bool Treap<_T>::remove(int &pos, _T key)
{
	if (pos==-1) return false;
	if (key<node[pos].key) return remove(node[pos].left, key);
	else if (key>node[pos].key) return remove(node[pos].right, key);
	else {
		if (node[pos].left==-1 && node[pos].right==-1)
			pos=-1;
		else if (node[pos].left==-1)
			pos = node[pos].right;
		else if (node[pos].right==-1)
			pos = node[pos].left;
		else {
			if (node[node[pos].left].weight>node[node[pos].right].weight) {
				rotate_r(pos);
				return remove(node[pos].right, key);
			} else {
				rotate_l(pos);
				return remove(node[pos].left, key);
			}
		}
	}
	return true;
}

template <typename _T>
void Treap<_T>::print(int &pos)
{
	if (pos == -1) return;
	print(node[pos].left);
	//	printf("%d: %d\n", node[pos].key, node[pos].weight);
	print(node[pos].right);
}